package com.telusko.tusk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.telusko.tusk.entity.Health;
@Repository
public class HealthService {
	Health health;
	public float healthServices(Health health)
	{
		//System.out.println("Healthsssss"+health);
				String name=health.getName();
				String gender=health.getGender();
				int age=health.getAge();
				String[] health1=health.getHealth();
				String[] habits=health.getHabits();
				double  amount=5000;
				float amountfinal=0;
				
				if( age < 18)
				{
					 amount=5000;
				}
				else if(age >=18 && age<=40)
				{
					amount+= amount * 0.10;
				}

				else if(age >=40)
				{
					amount+= amount * 0.20;
				}
				else if( gender.equals("M"))
				{
					amount+= amount * 0.02;
				}
				else
				{
					amount+=amount;
				}
				if(health1 != null)
				{
					for(int i=0; i<health1.length; i++)
					{
						if(health1[i].equals("Hypertension") || health1[i].equals("Blood sugar") || health1[i].equals("Overweight") || health1[i].equals("Blood pressure"))
						{
							amount+= amount * 0.01;
						}
					}
				}
				if(habits != null)
				{
				
				for(int i=0; i<habits.length; i++)
				{
					if(habits[i].equals("Alcohol") || habits[i].equals("Drugs") || habits[i].equals("Smoking"))
					{
						amount+= amount * 0.03;
					}
					else if(habits[i].equals("Daily exercise"))
					{
						amount-= amount * 0.03;
					}
				}
				}
				amountfinal = (float) Math.round(amount * 100) / 100;
				
				return amountfinal;
	}
}
