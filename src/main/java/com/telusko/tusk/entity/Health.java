package com.telusko.tusk.entity;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Health {

	@Id
	private int id;
	private String name;
	private String gender;
	private int age;
	private String[] habits;
	private String[] health;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String[] getHabits() {
		return habits;
	}
	public void setHabits(String[] habits) {
		this.habits = habits;
	}
	public String[] getHealth() {
		return health;
	}
	public void setHealth(String[] health) {
		this.health = health;
	}
	@Override
	public String toString() {
		return "Health [id=" + id + ", name=" + name + ", gender=" + gender + ", age=" + age + ", habits="
				+ Arrays.toString(habits) + ", health=" + Arrays.toString(health) + "]";
	}
	
	
	
}
