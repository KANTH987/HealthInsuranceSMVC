package com.telusko.tusk.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import com.telusko.tusk.entity.Health;
import com.telusko.tusk.service.HealthService;
@Repository
public class HealthDao {
	@Autowired
	private SessionFactory sessionFactory;
	Health health;
	@Autowired
	HealthService hservice;
	@Transactional
	public float healthInsuranceCheck1(Health health)
	{
		float amountfinal=hservice.healthServices(health);
		
		/*Session session = sessionFactory.getCurrentSession();
		//NativeQuery<Health> query = session.createNativeQuery(" insert into Health (age, gender, habits, health, name, id) values ("+age+",'"+gender+"', '"+habits+"', '"+health+"', '"+name+"', ''");
		session.save(health);*/
		return amountfinal;
		
	}
}
